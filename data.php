<?php

  $data = array(
    'Греция' => array(
      'name' => 'greece',
      'price' => '362$',
      'tour_to' => 'ГРЕЦИЮ',
      'red_line' => 'Весна на Средиземном море. <br><small>«Раннее бронирование» до <strong>50%</strong>!!!</small>',
      'tours' => array(
        /*
        */
        array(
          'img' => 'g/1.jpg',
          'name' => 'ARCO BALENO APARTMENTS 3*',
          'where' => 'Греция, Херсониссос',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'All',
          'price' => '362',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/9.jpg',
          'name' => 'GRECOTEL CARAMEL BEACH VILLAGE 4*',
          'where' => 'Греция, Аделе',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'HB',
          'price' => '630',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/10.jpg',
          'name' => 'GRECOTEL CLUB MARINE PALACE & SUITES 4*+',
          'where' => 'Греция, Панормо',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'ALL',
          'price' => '795',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/11.jpg',
          'name' => 'GRECOTEL CRETA PALACE 5*',
          'where' => 'Греция',
          'date' => '12.05, 7 ночей, ½ DBL',
          'eat' => 'HB',
          'price' => '760',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/12.jpg',
          'name' => 'GRECOTEL MELI PALACE 4*',
          'where' => 'Греция, Сисси',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'ALL',
          'price' => '720',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/13.jpg',
          'name' => 'GRECOTEL PLAZA SPA APTS 4*+',
          'where' => 'Греция',
          'date' => '12.05, 7 ночей, ½ DBL',
          'eat' => 'BB',
          'price' => '636',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/14.jpg',
          'name' => 'EKAVI HOTEL 3*',
          'where' => 'Греция',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'BB',
          'price' => '443',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        array(
          'img' => 'g/2.jpg',
          'name' => 'BRASCOS HOTEL 3*',
          'where' => 'Греция',
          'date' => '09.05, 7 ночей, ½ DBL',
          'eat' => 'BB',
          'price' => '451',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOWHER&Hotel=GR-ANS-ABA&CheckIn=12052013'
        ),
        /*
        array(
          'img' => 'tour.jpg',
          'name' => '',
          'where' => 'Турция, ',
          'date' => '',
          'eat' => '',
          'price' => '',
          'link' => ''
        ),
        */
      )
    ),
    'Турция' => array(
      'name' => 'turkey',
      'price' => '314$',
      'tour_to' => 'ТУРЦИЮ',
      'red_line' => 'Весна на Средиземном море. <br><small>«Раннее бронирование» до <strong>50%</strong>!!!</small>',
      'tours' => array(
        /*
        */
        array(
          'img' => 't/9.jpg',
          'name' => 'WOW KREMLIN PALACE 5*',
          'where' => 'Турция, Аланья',
          'date' => '15.04, 2 ночи, 1/2 DB',
          'eat' => 'UAL',
          'price' => '458',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/10.jpg',
          'name' => 'WOW TOPKAPI PALACE 5*',
          'where' => 'Турция, Аланья',
          'date' => '15.04, 2 ночи, 1/2 DB',
          'eat' => 'UAL',
          'price' => '491',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/11.jpg',
          'name' => 'ALARA HOTEL 4*',
          'where' => 'Турция, Аланья',
          'date' => '17.04, 2 ночи, 1/2 DB',
          'eat' => 'UAL',
          'price' => '460',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/5.jpg',
          'name' => 'UTOPIA WORLD HOTEL 5*',
          'where' => 'Турция, Аланья',
          'date' => '14.04, 2 ночи, 1/2 DB',
          'eat' => 'UAL',
          'price' => '505',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/12.jpg',
          'name' => 'SIDE ARIA HOTEL 3*+',
          'where' => 'Турция, Сиде',
          'date' => '17.04, 2 ночи, 1/2 DB',
          'eat' => 'AL',
          'price' => '301',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/13.jpg',
          'name' => 'MAYA GOLF HOTEL HV2',
          'where' => 'Турция, Сиде',
          'date' => '17.04, 2 ночи, 1/2 DB',
          'eat' => 'AL',
          'price' => '365',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/14.jpg',
          'name' => 'MARITIM PINE BEACH RESORT 5*',
          'where' => 'Турция, Белек',
          'date' => '17.04, 2 ночи, 1/2 DB',
          'eat' => 'UAL',
          'price' => '553',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/15.jpg',
          'name' => 'ADONIS HOTEL 3*',
          'where' => 'Турция, Кемер',
          'date' => '17.04, 2 ночи, 1/2 DB',
          'eat' => 'AL',
          'price' => '316',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        /*
        array(
          'img' => 't/1.jpg',
          'name' => 'LAKE & RIVER SIDE HOTEL&SPA 5*',
          'where' => 'Турция, Манавгат',
          'date' => '03.04, 2 ночи, 1/2 DB',
          'eat' => 'Uall',
          'price' => '314',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/2.jpg',
          'name' => 'HEDEF RESORT & SPA 5*',
          'where' => 'Турция, Аланья',
          'date' => '03.04, 2 ночи, 1/2 DB',
          'eat' => 'Uall',
          'price' => '321',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/3.jpg',
          'name' => 'LE CHATEAU DE PRESTIGE 5*',
          'where' => 'Турция, Кемер',
          'date' => '03.04, 2 ночи, 1/2 DB',
          'eat' => 'Uall',
          'price' => '333',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/4.jpg',
          'name' => 'GOLDCITY TOURISM COMPLEX 5*',
          'where' => 'Турция, Махмутлар',
          'date' => '03.04, 2 ночи, 1/2 DB',
          'eat' => 'All',
          'price' => '337',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        
        array(
          'img' => 't/6.jpg',
          'name' => 'CLUB GOLDEN BEACH 5*',
          'where' => 'Турция, Сиде',
          'date' => '03.04, 2 ночи, 1/2 DB',
          'eat' => 'Uall',
          'price' => '371',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/7.jpg',
          'name' => 'HILTON DALAMAN RESORT & SPA SCLASS',
          'where' => 'Турция, Сарыгерме',
          'date' => '14.05, 7 ночей, ½ DBL',
          'eat' => 'UAL',
          'price' => '1024',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),
        array(
          'img' => 't/8.jpg',
          'name' => 'DINARA HOTEL 3*',
          'where' => 'Турция, Кемер',
          'date' => '15.04, 7 ночей, 1/2 DB',
          'eat' => 'BB',
          'price' => '563',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-AYT&Hotel=TR-AYT-MRB&CheckIn=05042013'
        ),*/
        /*
        array(
          'img' => 'tour.jpg',
          'name' => '',
          'where' => 'Турция, ',
          'date' => '',
          'eat' => '',
          'price' => '',
          'link' => ''
        ),
        */
      )
    ),
    'Испания' => array(
      'name' => 'spain',
      'price' => '333€',
      'tour_to' => 'ИСПАНИЮ',
      'red_line' => 'Весна на Средиземном море. <br><small>«Раннее бронирование» до <strong>50%</strong>!!!</small>',
      'tours' => array(
        /*
        */
        array(
          'img' => 's/1.jpg',
          'name' => 'SANTA ROSA 3*',
          'where' => 'Испания, Льорет-де-Мар',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '333',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/3.jpg',
          'name' => 'MUNDIAL CLUB 3*',
          'where' => 'Испания, Льорет-де-Мар',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '336',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/9.jpg',
          'name' => 'NORAI 2*',
          'where' => 'Испания, Льорет-де-Мар',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'BB',
          'price' => '340',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/10.jpg',
          'name' => 'COSTA BRAVA BLANES 2*',
          'where' => 'Испания, Бланес',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'BB',
          'price' => '385',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/4.jpg',
          'name' => 'CASPEL 3*+',
          'where' => 'Испания, Салоу',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '395',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/5.jpg',
          'name' => 'PLAYA MARGARITA 3*',
          'where' => 'Испания, Салоу',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '410',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/11.jpg',
          'name' => 'PALAS PINEDA 4*',
          'where' => 'Испания, Ла Пинеда',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'BB',
          'price' => '426',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/12.jpg',
          'name' => 'PIRAMIDE SALOU 4*',
          'where' => 'Испания, Салоу',
          'date' => '18.04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '457',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        /*
        array(
          'img' => 's/2.jpg',
          'name' => 'MERCEDES 3*',
          'where' => 'Испания, Льорет-де-Мар',
          'date' => '09/04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '158',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/6.jpg',
          'name' => 'SANTA MONICA PLAYA 3*',
          'where' => 'Испания, Салоу',
          'date' => '09/04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '237',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/7.jpg',
          'name' => 'AQUA HOTEL AQUAMARINA 3*',
          'where' => 'Испания, Санта-Сусанна',
          'date' => '09/04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '252',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),
        array(
          'img' => 's/8.jpg',
          'name' => 'HELIOS 4*',
          'where' => 'Испания, Льорет-де-Мар',
          'date' => '09/04 на 7н./8дн. ½ DBL',
          'eat' => 'HB',
          'price' => '257',
          'link' => 'http://searchru1.anextour.com/SearchResult.aspx?Package=MOW-BCN3&Hotel=ES-CSB-AQU&CheckIn=07042013'
        ),*/
        /*
        array(
          'img' => 'tour.jpg',
          'name' => '',
          'where' => 'Турция, ',
          'date' => '',
          'eat' => '',
          'price' => '',
          'link' => ''
        ),
        */
      ),
    ),
  );

?>