$(document).ready(function() {
  links('*[data-link]');
});

function links (links) {
  $(links).each(function() {
    $(this).click(function() {
      location.href = $(this).attr('data-link');
      return false;
    });
  });
}