<?php
  include 'data.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
  <title></title>
  <link rel="stylesheet" type="text/css" href="application.css" media="all" />
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/application.js"></script>
</head>
<body class="index">
  <header>
    <a href="/anex_sp"><img src="i/index/logo.png" /></a>
  </header>
  <section>
    <h1>Лучшие пляжи Европы</h1>
    <?php foreach ($data as $key => $value): ?>
      <article class="<?php echo $value['name'] ?>" data-link="<?php echo $value['name'] ?>.php">
        <div class="image"></div>
        <p class="price">Туры от <span><?php echo $value['price'] ?></span></p>
        <p><a href="<?php echo $value['name'] ?>.php">Выбрать тур</a></p>
      </article>
    <?php endforeach ?>
    <div class="clear"></div>
  </section>
  <footer>
    <div class="copyright">Aнекс Тур © 2013</div>
    <div class="social">
      <a class="vk" href="http://vk.com/club20556958"></a>
    </div>
  </footer>
</body>
</html>